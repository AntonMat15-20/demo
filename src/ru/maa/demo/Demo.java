package ru.maa.demo;


import java.io.*;

/**
 * Created by Anton on 30.03.2017.
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));
        String string;
        while ((string = bufferedReader.readLine()) != null) {
            String[] strings = string.split(" ");
            int num1 = Integer.valueOf(strings[0]);
            int num2 = Integer.valueOf(strings[2]);
            char sym = strings[1].charAt(0);
            int result = calculation(num1, num2, sym);
            System.out.println(result);
            bufferedWriter.write(result + "\n");
            bufferedWriter.flush();
        }
        bufferedReader.close();
    }

    private static int calculation(int num1, int num2, char sym) {
        switch (sym) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '*':
                return num1 * num2;
            case '/':
                return num1 / num2;
        }
        return 0;
    }
}
